package sart.actor;

public class Reference {
	private final Record record;
	
	Reference(final Record record) {
		this.record = record;
	}
	
	public void send(String name, Object...args) {
		Worker.current().out(record, name, args);
	}
}
