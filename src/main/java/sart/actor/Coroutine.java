package sart.actor;

import java.util.ArrayList;

public class Coroutine {
	private volatile boolean lock = false;
	
	private ArrayList<Coroutine> andList = new ArrayList<>();
	
	private Coroutine then;
	
	public Coroutine and(Coroutine coroutine) {
		andList.add(coroutine);
		return this;
	}
	
	public Coroutine then(Coroutine coroutine) {
		if(!lock) {
			
		}
		return this;
	}
	
	final void lock() {
		lock = true;
	}
}
