package sart.actor.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface request {
	String behavior() default "";
	
	String name() default "";
}
