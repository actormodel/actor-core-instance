package sart.actor;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinWorkerThread;

public class Worker extends ForkJoinWorkerThread {
	private Record target, source;
	
	protected Worker(ForkJoinPool pool) {
		super(pool);
		setDaemon(false);
	}
	
	public static void execute(Task task) {
		ForkJoinPool.commonPool().execute(task::execute);
	}
	
	public static Worker current() {
		return (Worker) Thread.currentThread();
	}

	public Reference target() {
		return target.reference;
	}
	
	public Reference source() {
		return source.reference;
	}

	public void context(Record source, Record target) {
		this.source = source;
		this.target = target;
	}
	
	public void free() {
		target.free();
		target = null;
	}

	Coroutine spawn(String address, Object instance) {
		return null;
	}

	void out(Record target, String name, Object[] args) {
		target.in(this.target, name, args);
	}

	Reference create(String address, Object instance) {
		return target.create(address, instance);
	}

}
