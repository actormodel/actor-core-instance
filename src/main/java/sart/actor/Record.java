package sart.actor;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.MethodType;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Optional;

public class Record {
	private static final Lookup lookup = MethodHandles.publicLookup();
	
	private final Record parent;

	final Reference reference = new Reference(this);
	
	private final Object instance;
	
	final String name;
	
	final String address;
	
	private final HashMap<String, Behavior> btable = new HashMap<>();
	
	private final HashMap<String, Record> childs = new HashMap<>();
	
	private final LinkedList<Task> mailbox = new LinkedList<>();
	
	private Behavior behavior;
	
	private int state = StFree;
	
	private MethodHandle md = null;//define method
	
	public Record(Record parent, Object instance, String name) {
		this.parent = parent;
		this.instance = instance;
		this.address = (parent != null ? parent.address + "/" : "/") + name;
		this.name = name;
		//build behavior from instance	
		behavior = new Behavior();
		Arrays.asList(instance.getClass().getDeclaredMethods())
			.forEach(m -> {
				final int modifiers = m.getModifiers();
				if(Modifier.isPublic(modifiers) && !Modifier.isStatic(modifiers)) {
					try {
						MethodHandle mh = 
								lookup.bind(
										instance, 
										m.getName(),
										MethodType.methodType(
												m.getReturnType(), 
												m.getParameterTypes()
										)
									);
						if(m.isAnnotationPresent(sart.actor.annotation.define.class)) {
							md = mh;
						} else if(md == null && m.getName().equals("define")) {
							md = mh;
						} else {
							behavior.register(m.getName(), mh);
						}
						

					} catch (NoSuchMethodException | IllegalAccessException e) { e.printStackTrace(); }
				}
			});
	}
	
	synchronized void define() {
		if(md != null) {
			Worker.execute(new Task.MessageTask(this, this, md.toString(), md, new Object[]{}));
			state = StProcess;
		}
	}
	
	synchronized void in(Record source, String name, Object[] args) {
		final Task task = new Task.MessageTask(source, this, name, behavior.find(name).get(), args);
		if(state == StFree) {
			Worker.execute(task);
			state = StProcess;
		} else
			mailbox.add(task);
	}
	
	synchronized void free() {
		if(!mailbox.isEmpty()) {
			Worker.execute(mailbox.removeFirst());
			state = StProcess;
		} else
			state = StFree;
	}
	
	class Behavior {
		final HashMap<String, MethodHandle> handle = new HashMap<>();
		
		void register(String name, MethodHandle mh) {
			handle.put(name, mh);
		}
		
		Optional<MethodHandle> find(String name) {
			return Optional.ofNullable(handle.get(name));
		}
	}
	
	private final static int StFree = 0;
	
	private final static int StProcess = 1;

	public Reference create(String address, Object instance) {
		Record record = new Record(this, instance, address);
		synchronized (childs) {
			if(!childs.containsKey(address)) {
				childs.put(address, record);
				record.define();
			} else
				System.out.println("actor exists with address: " + this.address + "/" + address);
		}
		return record.reference;
	}

	public synchronized Record find(String name) {
		synchronized (childs) {
			if(childs.containsKey(name)) {
				return childs.get(name);
			}
		}
		return null;
	}
}
