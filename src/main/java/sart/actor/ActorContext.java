package sart.actor;

public interface ActorContext {

	default Reference self() {
		return Worker.current().target();
	}

	default Coroutine spawn(String address, Object instance) {
		return Worker.current().spawn(address, instance);
	}
	
	default Reference create(String address, Object instance) {
		return Worker.current().create(address, instance);
	}
	
	default Reference sender() {
		return Worker.current().source();
	}

}
