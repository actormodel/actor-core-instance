package sart.actor;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinPool.ForkJoinWorkerThreadFactory;

import java.util.concurrent.ForkJoinWorkerThread;

public class Model implements ForkJoinWorkerThreadFactory, ActorContext {
	private Record root = new Record(null, this, "actor-system");
	
	public void init(Object root) {
		create("root", root);
	}
	
	public static void launch() {
		System.setProperty("java.util.concurrent.ForkJoinPool.common.threadFactory", Model.class.getName());
	}

	@Override
	public ForkJoinWorkerThread newThread(ForkJoinPool pool) {
		return new Worker(pool);
	}

	public static void launch(Object root) {
		System.setProperty("java.util.concurrent.ForkJoinPool.common.threadFactory", Model.class.getName());
		((Model)ForkJoinPool.commonPool().getFactory()).root.in(null, "init", new Object[]{root});
	}

}
