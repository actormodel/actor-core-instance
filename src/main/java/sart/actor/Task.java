package sart.actor;

import java.lang.invoke.MethodHandle;

public abstract class Task {

	public abstract void execute();

	static class MessageTask extends Task {
		private final Record source;

		private final Record target;
		
		private final String name;
		
		private final MethodHandle mh;
		
		private final Object[] args;
		
		MessageTask(Record source, Record target, String name, MethodHandle mh, Object[] args) {
			this.source = source;
			this.target = target;
			this.name = name;
			this.mh = mh;
			this.args = args;
		}
		
		@Override
		public void execute() {
			Worker.current().context(source, target);
			try {
				mh.invokeWithArguments(args);	 
			} catch(Throwable e) { e.printStackTrace(); }
			Worker.current().free();
		}
	}
}
