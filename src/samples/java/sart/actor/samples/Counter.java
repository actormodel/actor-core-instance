package sart.actor.samples;

import sart.actor.ActorContext;
import sart.actor.Reference;
import sart.actor.annotation.actor;
import sart.actor.annotation.define;

@actor
public class Counter implements ActorContext {
	private int index;
	
	private int count = 0;
	
	private long time = System.currentTimeMillis() + 1000;
	
	public Counter(int index) {
		this.index = index;
	}
	
	@define
	public void define() {
		self().send("tick");
	}
	
	public void tick() {
		if(time <= System.currentTimeMillis()) {
			System.out.println("counter " + index + ": " + count);
			count = 0;
			time = System.currentTimeMillis() + 1000;
		} else
			count++;
		self().send("tick");
	}
	
}
