package sart.actor.samples;

import sart.actor.ActorContext;
import sart.actor.Model;

public class MultiCounter implements ActorContext {

	public void define() {
		for(int i = 0 ; i < 10000 ; i++)
			create("counter-" + i, new Counter(i));
	}
	
	public static void main(String...args) {
		Model.launch(new MultiCounter());
	}
}
