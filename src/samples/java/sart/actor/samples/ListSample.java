package sart.actor.samples;

import java.util.ArrayList;
import java.util.function.Consumer;

import sart.actor.ActorContext;
import sart.actor.Model;
import sart.actor.Reference;

public class ListSample implements ActorContext{

	public void define() {
		Consumer<Object> consumer = System.out::println;
		
		Consumer<Object> consumer1 = x -> System.out.println("2 " + x);
		
		ArrayList<Integer> list = new ArrayList<>();
		
		for(int i = 0 ; i < 1000000 ; i++)
			list.add(i);
		
		Reference  ref = create("list", list);
		
		ref.send("forEach", consumer);
		
		ref.send("forEach", consumer1);
		
		self().send("tick");
	}
	
	public void tick() {
		System.out.println("tick");
		self().send("tick");
	}
	
	public static void main(String...args) {
		Model.launch(new ListSample());
	}
}
