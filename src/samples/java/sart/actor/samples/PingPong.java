package sart.actor.samples;

import sart.actor.ActorContext;
import sart.actor.Model;
import sart.actor.Reference;

public class PingPong implements ActorContext {

	public void define() {
		create("pong", new Pong()).send("start", create("ping", new Ping()));
	}
	
	public class Pong {
		
		public void start(Reference target) {
			target.send("pong");
		}
		
		public void ping() {
			System.out.println("ping");
			sender().send("pong");
		}
	}
	
	public class Ping {
	
		public void pong() {
			System.out.println("pong");
			sender().send("ping");
		}
	}
	
	public static void main(String...args) {
		Model.launch(new PingPong());
	}
}
